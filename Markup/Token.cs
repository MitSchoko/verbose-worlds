﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace MitSchoko.VerboseWorlds.Markup
{
    public class Token
    {
        public string Type;
        public Dictionary<string, string> Attributes = null;
        public string Content;
        public List<Token> Children = new List<Token>();
        public Token Parent;
        public Markup Tokenizer;
        public Effect.Instance UsedEffect;
        public void SubDivideContent()
        {
            if (string.IsNullOrEmpty(Content)) return;
            MatchCollection tokenMatches = Tokenizer.TokenRegex.Matches(Content);
            int lastIndex = 0;
            foreach (Match tokenMatch in tokenMatches)
            {
                if (tokenMatch.Index > lastIndex)
                {
                    Children.Add(
                        new Token(
                            Tokenizer,
                            this,
                            Content.Substring(
                                lastIndex,
                                tokenMatch.Index - lastIndex
                                )
                            )
                        );
                }
                Children.Add(
                    new Token(
                        Tokenizer,
                        this,
                        tokenMatch.Groups["token"].Value,
                        Tokenizer.ParseAttributes(tokenMatch.Groups["attributes"].Value),
                        tokenMatch.Groups["content"].Value
                        )
                    );
                lastIndex = tokenMatch.Index + tokenMatch.Length;
            }

            if (Children.Count > 0 && lastIndex < Content.Length - 1)
            {
                Children.Add(
                        new Token(
                            Tokenizer,
                            this,
                            Content.Substring(
                                lastIndex
                            )
                        )
                );
            }

        }

        public Token(Markup tokenizer, Token parent, string content)
        {
            this.Tokenizer = tokenizer;
            this.Parent = parent;
            this.Content = content;
            SubDivideContent();
        }

        public Token(Markup tokenizer, Token parent, string type, Dictionary<string, string> attributes, string content)
        {
            Tokenizer = tokenizer;
            Parent = parent;
            Type = type;
            Attributes = attributes;
            Content = content;
            SubDivideContent();
            UsedEffect = Tokenizer.GetTokenEffect(Type, Attributes);

        }

        public bool IsAtomic
        {
            get
            {
                return Children == null || Children.Count == 0;
            }
        }

        public void Render(RenderingContext context)
        {
            _Render(context);
            context.EndSegment();
        }
        void _Render(RenderingContext context)
        {
            UsedEffect?.RenderStart(this, context);
            if (IsAtomic)
            {
                if (UsedEffect != null)
                {
                    context.AppendTimed(UsedEffect.RenderContent(Content, context));
                }
                else
                {
                    context.AppendTimed(Content);
                }
            }
            else
            {
                foreach (var child in Children)
                {
                    if (!context.hasTimeLeft) break;
                    child.Render(context);
                }
            }
            UsedEffect?.RenderEnd(this, context);
        }
    }
}