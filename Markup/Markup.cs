﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName = "Verbose Worlds Markup/Markup Settings", order = -2)]
    public class Markup : ScriptableObject
    {
        #region Parsing
        [Header("Regex")]
        [SerializeField]
        string _TokenRegexPattern = @"<(?<token>\w+?)(\s(?<attributes>.+?))?\s*(/>|>(?<content>.*?)</\k<token>>)";
        Regex _TokenRegex;
        public Regex TokenRegex
        {
            get
            {
                if (
                    _TokenRegex == null
                    && !string.IsNullOrEmpty(_TokenRegexPattern)
                    )
                {
                    _TokenRegex = new Regex(_TokenRegexPattern);
                }
                return _TokenRegex;
            }
        }

        [SerializeField]
        string _AttributeRegexPattern = @"(?<key>.+?)=(?<value>(.+?\s|.+?$))";
        Regex _AttributeRegex;
        public Regex AttributeRegex
        {
            get
            {
                if (
                    _AttributeRegex == null
                    && !string.IsNullOrEmpty(_AttributeRegexPattern)
                    )
                {
                    _AttributeRegex = new Regex(_AttributeRegexPattern);
                }
                return _AttributeRegex;
            }
        }

        public Token TokenizeString(string content)
        {
            return new Token(this, null, content);
        }
        #endregion

        #region Token Definitions
        [Header("Token Definitions")]
        [SerializeField]
        List<TokenDefinition> TokenDefinitions = new List<TokenDefinition>();

        [System.Serializable]
        public class TokenDefinition
        {
            [SerializeField]
            public string Name;
            [SerializeField]
            public Effect Effect;
        }

        public Dictionary<string, string> ParseAttributes(string attributeString)
        {
            MatchCollection matches = AttributeRegex.Matches(attributeString);
            if (matches.Count == 0) return null;
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (Match match in matches)
            {
                result.Add(match.Groups["key"].Value.Trim(), match.Groups["value"].Value.Trim());
            }
            return result;
        }

        public Effect.Instance GetTokenEffect(string type, Dictionary<string, string> parameters)
        {

            if (string.IsNullOrEmpty(type)) return null;
            foreach (var td in TokenDefinitions)
            {
                if (td.Name == type)
                {
                    return td.Effect.CreateInstance(parameters);
                }
            }
            return null;
        }
        #endregion
    }
}