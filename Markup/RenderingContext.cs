﻿using System.Collections.Generic;
using System.Text;

namespace MitSchoko.VerboseWorlds.Markup
{
    public class RenderingContext
    {
        public string result
        {
            get
            {
                if (TextSegments.Count == 0) return string.Empty;
                else if (TextSegments.Count == 1) return TextSegments[0];
                else return string.Join(string.Empty, TextSegments);
            }
        }
        public TypeWriterSettings typeWriterSettings;
        public bool hasTypeWriterSettings => typeWriterSettings != null;
        public bool hasTimeLeft => !hasTypeWriterSettings || remainingTimeSeconds > 0.0f;
        public List<string> TextSegments = new List<string>();

        public void Append(string text) => _builder.Append(text);

        public void AppendTimed(string text)
        {
            if (hasTypeWriterSettings)
            {
                var result = typeWriterSettings.GetSubstringForTime(text, remainingTimeSeconds, out remainingTimeSeconds);
                _builder.Append(result);
            }
            else
            {
                _builder.Append(text);
            }
        }
        public void EndSegment()
        {
            TextSegments.Add(_builder.ToString());
            _builder.Clear();
        }
        public float timeSeconds { get; private set; }
        public float remainingTimeSeconds;
        StringBuilder _builder = new StringBuilder();

        public void Reset(float timeSeconds = 0.0f)
        {
            TextSegments.Clear();
            this.timeSeconds = timeSeconds;
            remainingTimeSeconds = this.timeSeconds;
        }
    }
}