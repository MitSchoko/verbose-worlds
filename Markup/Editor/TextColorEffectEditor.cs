﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace MitSchoko.VerboseWorlds.Markup
{
    [CustomEditor(typeof(TextColorEffect))]
    public class TextColorEffectEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            TextColorEffect t = target as TextColorEffect;
            if (t.type == TextColorEffect.Type.Fix)
            {
                DrawPropertiesExcluding(serializedObject, "GradientColor", "GradientDuration");
            }
            else
            {
                DrawPropertiesExcluding(serializedObject, "FixColor");
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}