﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;
namespace MitSchoko.VerboseWorlds.Markup
{
    [CustomEditor(typeof(Markup))]
    public class TokenizerEditor : Editor
    {
        public string TestInput;
        public Markup tokenizer
        {
            get
            {
                return target as Markup;
            }
        }
        public Token TestResult = null;
        public bool ShowTokenizerDebugger = false;
        float _renderTime = 0.0f;
        RenderingContext _context = new RenderingContext();
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            ShowTokenizerDebugger = EditorGUILayout.Foldout(ShowTokenizerDebugger, "Debug Tokenization");
            if (!ShowTokenizerDebugger) return;
            TestInput = EditorGUILayout.TextArea(TestInput, GUILayout.Height(100));
            _context.typeWriterSettings = (TypeWriterSettings)EditorGUILayout.ObjectField(
                _context.typeWriterSettings,
                typeof(TypeWriterSettings),
                false
            );
            _renderTime = EditorGUILayout.Slider(_renderTime, 0, 100);
            TestResult = tokenizer.TokenizeString(TestInput);
            DrawTokenStructure(TestResult);

            _context.Reset(_renderTime);
            TestResult.Render(_context);
            EditorGUILayout.TextArea(_context.result);
        }

        public static void DrawTokenStructure(Token token)
        {
            GUIUtils.Header(string.IsNullOrEmpty(token.Type) ? "-" : token.Type);
            EditorGUI.indentLevel++;
            if (!string.IsNullOrEmpty(token.Content)) EditorGUILayout.LabelField(token.Content);
            foreach (var child in token.Children) DrawTokenStructure(child);
            EditorGUI.indentLevel--;
        }
    }
}