﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName = "Verbose Worlds Markup/Type Writer Settings", order = -1)]
    public class TypeWriterSettings : ScriptableObject
    {
        public float defaultDurationSeconds = 0.0f;
        [System.Serializable]
        public class CharacterClass
        {
            public string Characters;
            public float durationSeconds;
        }
        [SerializeField]
        public List<CharacterClass> SpecialCharacters = new List<CharacterClass>();

        public float SecondsForCharacter( char character )
        {

            foreach ( var sc in SpecialCharacters )
            {
                foreach ( var c in sc.Characters )
                {
                    if (c == character ) return sc.durationSeconds;
                }
            }
            return defaultDurationSeconds;
        }

        public string GetSubstringForTime( string fullStr, float timeSeconds, out float remainingTimeSeconds )
        {
            remainingTimeSeconds = timeSeconds;
            if (string.IsNullOrEmpty(fullStr)) return fullStr;
            if (SpecialCharacters.Count == 0)
            {
                if (timeSeconds <= 0) return string.Empty;
                float charactersForTime = timeSeconds / defaultDurationSeconds;
                int characters = Mathf.CeilToInt( charactersForTime );
                if (characters >= fullStr.Length)
                {
                    remainingTimeSeconds -= fullStr.Length * defaultDurationSeconds; 
                    return fullStr;
                }
                remainingTimeSeconds = 0;
                return fullStr.Substring(0, characters);
            } else
            {
                float timeCounter = 0.0f;
                for (int i = 0; i != fullStr.Length; ++i)
                {
                    timeCounter += SecondsForCharacter(fullStr[i]);
                    if (timeCounter >= timeSeconds)
                    {
                        remainingTimeSeconds = 0;
                        return fullStr.Substring(0, i + 1);
                    }
                }
                remainingTimeSeconds -= timeCounter;
                return fullStr;
            }
        }
    }
}