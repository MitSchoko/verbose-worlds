﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
namespace MitSchoko.VerboseWorlds.Markup {
    public abstract class Effect : ScriptableObject
    {

        public abstract Instance CreateInstance(IDictionary<string, string> parameters);


        public class Instance
        {
            public virtual void RenderStart(Token token, RenderingContext context) { }
            public virtual bool modifiesContent => false;
            public virtual string RenderContent(string original, RenderingContext context) { return original; }
            public virtual void RenderEnd(Token token, RenderingContext context) { }
        }
    }
}