﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName ="Verbose Worlds Markup/Effects/Color")]
    public class TextColorEffect : Effect
    {
        public enum Type
        {
            Fix,
            Gradient
        }
        public Type type = Type.Fix;
        public Color FixColor;
        public Gradient GradientColor;
        public float GradientDuration = 1.0f;
        public Color GetColor(RenderingContext context)
        {
            if (type == Type.Fix)
            {
                return FixColor;
            } else
            {
                return GradientColor.Evaluate((context.timeSeconds % GradientDuration) / GradientDuration);
            }
        }

        public override Effect.Instance CreateInstance(IDictionary<string, string> parameters)
        {
            return new Instance(this);
        }

        new public class Instance : Effect.Instance
        {
            TextColorEffect parent;
            public Instance( TextColorEffect parent)
            {
                this.parent = parent;
            }
            public override void RenderStart(Token token, RenderingContext context)
            {
                context.Append($"<color=#{ ColorUtility.ToHtmlStringRGB(parent.GetColor(context)) }>");
            }

            public override void RenderEnd(Token token, RenderingContext context)
            {
                context.Append("</color>");
            }
        }
    }
}