﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName = "Verbose Worlds Markup/Effects/Replace Text")]
    public class ReplaceEffect : Effect
    {
        [TextArea]
        public string Replacement;

        public override Effect.Instance CreateInstance(IDictionary<string, string> parameters)
        {
            return new Instance(this);
        }

        new public class Instance : Effect.Instance
        {
            public Instance( ReplaceEffect parent)
            {
                this.parent = parent;
            }
            ReplaceEffect parent;
            public override bool modifiesContent => true;
            public override string RenderContent(string original, RenderingContext context)
            {
                return parent.Replacement;
            }
        }
    }
}
