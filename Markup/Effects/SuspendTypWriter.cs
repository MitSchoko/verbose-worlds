﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName = "Verbose Worlds Markup/Effects/Suspend Type Writer")]
    public class SuspendTypWriter : Effect
    {
        public override Effect.Instance CreateInstance(IDictionary<string, string> parameters)
        {
            return new Instance();
        }

        new public class Instance : Effect.Instance
        {
            TypeWriterSettings backup;
            public override void RenderStart(Token token, RenderingContext context)
            {
                backup = context.typeWriterSettings;
                context.typeWriterSettings = null;
            }
            public override void RenderEnd(Token token, RenderingContext context)
            {
                context.typeWriterSettings = backup;
            }
        }
    }
}