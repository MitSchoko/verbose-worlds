﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace MitSchoko.VerboseWorlds.Markup
{
    [CreateAssetMenu(menuName = "Verbose Worlds Markup/Effects/Type Writer Wait")]
    public class TypeWriterWait : Effect
    {
        public float defaultTime = 1.0f;
        public string timeParameterName = "time";
        public override Effect.Instance CreateInstance(IDictionary<string, string> parameters)
        {
            float usedTime = defaultTime;
            if (! string.IsNullOrEmpty(timeParameterName) && parameters.ContainsKey(timeParameterName)) {
                float.TryParse(parameters[timeParameterName], NumberStyles.Float, CultureInfo.InvariantCulture, out usedTime);
            }
            return new Instance(usedTime);
        }

        new public class Instance : Effect.Instance {
            public Instance( float time )
            {
                this.time = time;
            }
            public float time;
            public override void RenderStart(Token token, RenderingContext context)
            {
                context.remainingTimeSeconds -= time;
            }
        }
    }
}