﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MitSchoko.VerboseWorlds.Markup;

namespace MitSchoko.VerboseWorlds {
    public class LabelTMPro : LabelAbstract
    {
        [SerializeField]
        TextMeshPro _textMeshPro;
        public TextMeshPro textMeshPro {
            get
            {
                if (_textMeshPro == null)
                {
                    _textMeshPro = GetComponent<TextMeshPro>();
                }
                return _textMeshPro;
            }
        }

        public override string text
        {
            get
            {
                return textMeshPro.text;
            }
            set
            {
                textMeshPro.text = value;
            }
        }

    }
}