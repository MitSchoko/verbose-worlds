﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MitSchoko.VerboseWorlds.Markup;
namespace MitSchoko.VerboseWorlds
{
    public abstract class LabelAbstract : MonoBehaviour
    {
        public TypeWriterSettings typeWriterSettings;
        public Markup.Markup markup;
        public abstract string text { get; set; }
        public virtual void ShowText(string text, float durationSeconds = -1.0f, Markup.Markup overrideMarkup = null, TypeWriterSettings overrideTypeWriterSettings = null)
        {
            TypeWriterSettings tws = overrideTypeWriterSettings ?? typeWriterSettings;
            Markup.Markup m = overrideMarkup ?? markup;
            Token root = m.TokenizeString(text);
            StartCoroutine(_ShowCoroutine(root, tws, durationSeconds));
        }

        IEnumerator _ShowCoroutine(Token token, TypeWriterSettings typeWriter, float durationSeconds = -1)
        {
            RenderingContext context = new Markup.RenderingContext();
            context.typeWriterSettings = typeWriter;
            float passedSeconds = VerboseWorlds.timeScale * Time.deltaTime;
            do
            {
                context.Reset(passedSeconds);
                token.Render(context);
                text = context.result;
                yield return null;
                passedSeconds += VerboseWorlds.timeScale * Time.deltaTime;
            } while (durationSeconds < 0 || context.remainingTimeSeconds < durationSeconds );
        }
    }
}