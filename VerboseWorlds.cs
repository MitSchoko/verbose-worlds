﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MitSchoko.VerboseWorlds
{
    public static class VerboseWorlds
    {
        static float _timeScale = 1.0f;
        public static float timeScale
        {
            get
            {
                return _timeScale;
            }
            set
            {
                _timeScale = Mathf.Max(0.0f, value);
            }
        }
    }
}