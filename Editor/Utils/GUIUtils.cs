﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MitSchoko
{
    public class GUIUtils
    {
        static Color BackgroundBackupColor { get; set; }

        public static void BackupBackgroundColor()
        {
            BackgroundBackupColor = GUI.backgroundColor;
        }

        public static void RestoreBackgroundColor()
        {
            GUI.backgroundColor = BackgroundBackupColor;
        }

        public static void TintBackground( Color color, float factor = 0.2f )
        {
            BackupBackgroundColor();
            GUI.backgroundColor = Color.Lerp(GUI.backgroundColor, color, factor);
        }

        public static void Header(string content)
        {
            EditorGUILayout.LabelField(content, EditorStyles.boldLabel);
        }
    }
}