﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System;

namespace MitSchoko.VerboseWorlds
{
    public class RegexTester : EditorWindow
    {
        [MenuItem("Verbose Worlds/Tools/RegexTester")]
        public static void Init()
        {
            GetWindow<RegexTester>().Show();

        }
        Vector2 _ScrollPosition;
        public string pattern;
        public string input;
        MatchCollection _matchCollection;
        bool _validPattern;
        string _errorMsg;



        public void OnGUI()
        {
            Regex regex = null;
            try
            {
                regex = new Regex(pattern);
                _validPattern = true;
            }
            catch (Exception ex)
            {
                _errorMsg = ex.Message;
                _validPattern = false;
            }

            GUIUtils.Header("Pattern");
            GUIUtils.TintBackground((_validPattern) ? Color.green : Color.red);
            pattern = EditorGUILayout.TextArea(pattern, GUILayout.Height(100.0f));
            GUIUtils.RestoreBackgroundColor();
            if (!_validPattern || regex == null)
            {
                EditorGUILayout.HelpBox(_errorMsg, MessageType.Error, true);
                return;
            }
            GUIUtils.Header("Input");
            input = EditorGUILayout.TextArea(input, GUILayout.Height(100.0f));
            if (!_validPattern || string.IsNullOrEmpty(input)) return;
            GUIUtils.Header("Results");

            _matchCollection = regex.Matches(input);
            EditorGUI.indentLevel++;
            int matchCounter = 0;
            _ScrollPosition = EditorGUILayout.BeginScrollView(_ScrollPosition);
            string[] keys = regex.GetGroupNames();
            foreach (Match match in _matchCollection)
            {

                GUIUtils.Header("Match[" + (matchCounter++) + "]");
                EditorGUI.indentLevel++;
                foreach (string key in keys)
                {
                    Group group = match.Groups[key];
                    EditorGUILayout.TextField(key, group.Value);
                }
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndScrollView();
        }
    }
}
